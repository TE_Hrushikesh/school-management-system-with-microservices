package com.te.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@OpenAPIDefinition
public class ExamcellApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamcellApplication.class, args);
	}

}
