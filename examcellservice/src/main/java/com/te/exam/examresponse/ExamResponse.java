package com.te.exam.examresponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamResponse {

	private boolean error;
	private String message;
	private Object data;
}
