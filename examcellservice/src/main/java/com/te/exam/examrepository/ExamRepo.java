package com.te.exam.examrepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.exam.examentity.Exam;

@Repository
public interface ExamRepo extends JpaRepository<Exam, Integer> {

	List<Exam> findByStudentId(String studentId);
}
