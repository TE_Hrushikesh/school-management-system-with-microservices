package com.te.exam.constants;

public class Constants {

	public static final String DELETING_RESULTS_SUCCESFULLY = "Deleting Results Succesfully";

	public static final String GETTING_FINAL_RESULT_SUCCESSFULLY = "Getting Final Result Successfully";

	public static final String GETTING_SEMESTER_RESULTS_SUCCESFULLY = "Getting Semester Results Succesfully";

	public static final String GETTING_UNIT_TEST_RESULT_SUCCESFULLY = "Getting Unit Test Result Succesfully";

	public static final String GETTING_EXCEPTION_OF_RESILENCE4J = "Getting Exception Of Resilence4j";

	public static final String GETTING_ALL_RESULTS_OF_PERTICULAR_STUDENT = "Getting All Results Of Perticular Student : ";

	public static final String DELETING_RESULTS_BY_EXAM_TYPE = "Deleting Results By Exam Type : ";

	public static final String GETTING_RESULT_LIST_BY_SCHOOL_ID = "Getting  Result List By School ID ";

	public static final String GETTING_ALL_RESULT_LIST = "Getting All Result List";

	public static final String GETTING_SEMESTER_TEST_RESULT_WITH = "Getting Semester Test Result with ";

	public static final String GETTING_UNIT_TEST_RESULT_WITH = "Getting Unit Test Result with ";

	public static final String GETTING_FINAL_TEST_RESULT_WITH = "Getting Final Test Result with ";

	public static final String ADDING_RESULT = "Adding Result";

	public static final String SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME2 = "SomeThing Went Wrong Please Try After Some Time!!!";

	public static final String GETTING_ALL_RESULTS_BY_STUDENT_ID = "Getting All Results By Student Id";

	public static final String GETTING_ALL_RESULTS_BY_SCHOOL_ID = "Getting All Results By School Id";

	public static final String GETTING_ALL_RESULTS_SUCCESFULLY = "Getting All Results Succesfully ";

	public static final String GETTING_RESULT_SUCCESFULLY_FOR_FINAL_TEST = "Getting Result Succesfully For Final Test ";

	public static final String GETTING_RESULT_SUCCESFULLY_FOR_SEMISTER = "Getting Result Succesfully For Semister : ";

	public static final String GETTING_RESULT_SUCCESFULLY_FOR_UNIT_TEST = "Getting Result Succesfully For Unit Test : ";

	public static final String RESULT_ADDED_SUCCESFULLY = "Result Added Succesfully";

	public static final String RESLUT_DELETED_SUCCESFULLY = "Reslut Deleted Succesfully";

	public static final String RESULT_NOT_DECLAIRED_YET = "Result Not Declaired Yet !!!!";

	public static final String RESULT_DATA_IS_EXPIRED = "Result Data Is Expired";

	public static final String RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER = "Result Not Declaired Yet Or You Entered Wrong Test Number!!!!";

	public static final String INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS = "Invalid Student ID Or Email ID Please Enter The Valid Ids";

	public static final String RESULT_ADDED_PREVIOUSLY_NO_NEED_TO_ADD = "Result Added Previously No Need To Add";

	public static final String YOU_ENTERED_THE_WRONG_EXAM_TYPE_OR_EXAM_NUMBER = "You Entered The Wrong Exam Type Or Exam Number";

}
