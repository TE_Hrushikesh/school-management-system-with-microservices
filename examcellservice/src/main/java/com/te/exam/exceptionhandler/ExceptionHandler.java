package com.te.exam.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.te.exam.customexception.CustomException;
import com.te.exam.examresponse.ExamResponse;

@RestControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(CustomException.class)
	public ResponseEntity<ExamResponse> customExceptionHandler(CustomException exception) {
		return new ResponseEntity<ExamResponse>(new ExamResponse(true, exception.getMessage(), null),
				HttpStatus.BAD_REQUEST);
	}
}
