package com.te.exam.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "STUDENT-SERVICES")
public interface StudentFeign {

	@GetMapping("/feignStudent/get/{studentId}")
	public com.te.exam.examdto.StudentDto getStudent(@PathVariable String studentId); 
}
