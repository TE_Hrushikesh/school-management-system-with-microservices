package com.te.exam.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.te.exam.examdto.SchoolDto;

@FeignClient(name = "SCHOOL-SERVICE")
public interface SchoolFeign {

	@GetMapping("/schoolFeign/getSchool/{schoolId}")
	public SchoolDto getSchool(@PathVariable String schoolId);
}