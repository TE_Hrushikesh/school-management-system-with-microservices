package com.te.exam.examentity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.te.exam.customexception.MaptoStringConverter;

import lombok.Data;

@Entity
@Data
public class Exam {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Transient
	private LocalDate date;
	private String examId;
	private String schoolId;
	private String schoolName;
	private String schoolEmail;
	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String examType;
	private boolean isDelete;

	@Convert(converter = MaptoStringConverter.class)
	@Column(columnDefinition = "TEXT")
	private Object subjectAndMarks;
	private int totalMarksObtained;
	private int totalMarks;
	private double percentage;
	private String result;
}
