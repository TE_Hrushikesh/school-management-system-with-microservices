package com.te.exam.examservice;

import static com.te.exam.constants.Constants.DELETING_RESULTS_SUCCESFULLY;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_BY_SCHOOL_ID;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_BY_STUDENT_ID;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_SUCCESFULLY;
import static com.te.exam.constants.Constants.GETTING_FINAL_RESULT_SUCCESSFULLY;
import static com.te.exam.constants.Constants.GETTING_SEMESTER_RESULTS_SUCCESFULLY;
import static com.te.exam.constants.Constants.GETTING_UNIT_TEST_RESULT_SUCCESFULLY;
import static com.te.exam.constants.Constants.INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS;
import static com.te.exam.constants.Constants.RESULT_ADDED_PREVIOUSLY_NO_NEED_TO_ADD;
import static com.te.exam.constants.Constants.RESULT_ADDED_SUCCESFULLY;
import static com.te.exam.constants.Constants.RESULT_DATA_IS_EXPIRED;
import static com.te.exam.constants.Constants.RESULT_NOT_DECLAIRED_YET;
import static com.te.exam.constants.Constants.RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER;
import static com.te.exam.constants.Constants.YOU_ENTERED_THE_WRONG_EXAM_TYPE_OR_EXAM_NUMBER;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.exam.customexception.CustomException;
import com.te.exam.examdto.ExamDto;
import com.te.exam.examdto.SchoolDto;
import com.te.exam.examdto.StudentDto;
import com.te.exam.examentity.Exam;
import com.te.exam.examrepository.ExamRepo;
import com.te.exam.feign.SchoolFeign;
import com.te.exam.feign.StudentFeign;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExamServiceImpl implements ExamService {

	@Autowired
	private ExamRepo repo;

	@Autowired
	private StudentFeign studFeign;

	@Autowired
	private SchoolFeign schoolFeign;

	@Override
	public Exam addresult(ExamDto examDto) {
		try {
			List<Exam> collect = repo.findAll().stream()
					.filter(t -> t.getExamType().equalsIgnoreCase(examDto.getExamType())
							&& t.getExamId().equalsIgnoreCase(examDto.getExamId()))
					.collect(Collectors.toList());
			if (!collect.isEmpty()) {
				log.error(RESULT_ADDED_PREVIOUSLY_NO_NEED_TO_ADD);
				throw new CustomException(RESULT_ADDED_PREVIOUSLY_NO_NEED_TO_ADD);
			} else {
				SchoolDto school = schoolFeign.getSchool(examDto.getSchoolId());
				BeanUtils.copyProperties(school, examDto);
				StudentDto student = studFeign.getStudent(examDto.getStudentId());
				BeanUtils.copyProperties(student, examDto);
				examDto.setTotalMarksObtained(examDto.getSubjecMarks().entrySet().stream()
						.collect(Collectors.summingInt(value -> (int) value.getValue())));
				examDto.setTotalMarks((int) examDto.getSubjecMarks().entrySet().stream().count() * 100);
				examDto.setPercentage((examDto.getTotalMarksObtained() * 100) / examDto.getTotalMarks());
				examDto.setResult(
						examDto.getSubjecMarks().entrySet().stream().anyMatch(t -> (int) t.getValue() < 35) ? "Fail"
								: "Pass");
				examDto.setDate(LocalDate.now());
				Exam exam = new Exam();
				exam.setSubjectAndMarks(examDto.getSubjecMarks());
				BeanUtils.copyProperties(examDto, exam);
				log.info(RESULT_ADDED_SUCCESFULLY);
				return repo.save(exam);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public Exam getUnitTestResult(String studentId, String emailId, String testNumber) {
		try {
			List<Exam> collect = repo.findByStudentId(studentId).stream()
					.filter(t -> t.getStudentEmail().equalsIgnoreCase(emailId)).collect(Collectors.toList());
			if (collect.isEmpty()) {
				log.error(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
				throw new CustomException(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
			} else {
				Optional<Exam> exam = collect.stream().filter(
						t -> t.getExamId().contains(testNumber) && t.getExamType().toLowerCase().contains("unit test"))
						.findFirst();
				if (!exam.isPresent()) {
					log.error(RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER);
					throw new CustomException(RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER);
				} else {
					if (exam.get().isDelete() == true) {
						log.error(RESULT_DATA_IS_EXPIRED);
						throw new CustomException(RESULT_DATA_IS_EXPIRED);
					} else {
						exam.get().setDate(LocalDate.now());
						log.info(GETTING_UNIT_TEST_RESULT_SUCCESFULLY);
						return exam.get();
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public Exam getSemesterResult(String studentId, String emailId, String testNumber) {
		try {
			List<Exam> collect = repo.findByStudentId(studentId).stream()
					.filter(t -> t.getStudentEmail().equalsIgnoreCase(emailId)).collect(Collectors.toList());
			if (collect.isEmpty()) {
				log.error(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
				throw new CustomException(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
			} else {
				Optional<Exam> exam = collect.stream().filter(
						t -> t.getExamId().contains(testNumber) && t.getExamType().toLowerCase().contains("semester"))
						.findFirst();
				if (!exam.isPresent()) {
					log.error(RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER);
					throw new CustomException(RESULT_NOT_DECLAIRED_YET_OR_YOU_ENTERED_WRONG_TEST_NUMBER);
				} else {
					if (exam.get().isDelete() == true) {
						log.error(RESULT_DATA_IS_EXPIRED);
						throw new CustomException(RESULT_DATA_IS_EXPIRED);
					} else {
						exam.get().setDate(LocalDate.now());
						log.info(GETTING_SEMESTER_RESULTS_SUCCESFULLY);
						return exam.get();
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public Exam getFinalResult(String studentId, String emailId) {
		try {
			List<Exam> collect = repo.findByStudentId(studentId).stream()
					.filter(t -> t.getStudentEmail().equalsIgnoreCase(emailId)).collect(Collectors.toList());
			if (collect.isEmpty()) {
				log.error(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
				throw new CustomException(INVALID_STUDENT_ID_OR_EMAIL_ID_PLEASE_ENTER_THE_VALID_IDS);
			} else {
				Optional<Exam> exam = collect.stream().filter(t -> t.getExamType().toLowerCase().contains("final test"))
						.findFirst();
				if (!exam.isPresent()) {
					log.error(RESULT_NOT_DECLAIRED_YET);
					throw new CustomException(RESULT_NOT_DECLAIRED_YET);
				} else {
					if (exam.get().isDelete() == true) {
						log.error(RESULT_DATA_IS_EXPIRED);
						throw new CustomException(RESULT_DATA_IS_EXPIRED);
					} else {
						exam.get().setDate(LocalDate.now());
						log.info(GETTING_FINAL_RESULT_SUCCESSFULLY);
						return exam.get();
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<Exam> getAllResults() {
		try {
			log.info(GETTING_ALL_RESULTS_SUCCESFULLY);
			return repo.findAll().stream().filter(t -> t.isDelete() == false).map(t -> {
				t.setDate(LocalDate.now());
				return t;
			}).collect(Collectors.toList());
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<Exam> getAllResultsBySchoolId(String schoolId) {
		try {
			log.info(GETTING_ALL_RESULTS_BY_SCHOOL_ID);
			return repo.findAll().stream()
					.filter(t -> t.getSchoolId().equalsIgnoreCase(schoolId) && t.isDelete() == false).map(t -> {
						t.setDate(LocalDate.now());
						return t;
					}).collect(Collectors.toList());
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<Exam> getStudentResultListById(String studentId) {
		try {
			log.info(GETTING_ALL_RESULTS_BY_STUDENT_ID);
			return repo.findAll().stream()
					.filter(t -> t.getStudentId().equalsIgnoreCase(studentId) && t.isDelete() == false).map(t -> {
						t.setDate(LocalDate.now());
						return t;
					}).collect(Collectors.toList());
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void deleteResult(String examType, String examNumber) {
		try {
			List<Exam> collect = repo.findAll().stream()
					.filter(t -> t.getExamType().equalsIgnoreCase(examType) && t.getExamId().contains(examNumber))
					.collect(Collectors.toList());
			if (collect.isEmpty()) {
				log.error(YOU_ENTERED_THE_WRONG_EXAM_TYPE_OR_EXAM_NUMBER);
				throw new CustomException(YOU_ENTERED_THE_WRONG_EXAM_TYPE_OR_EXAM_NUMBER);
			} else {
				log.info(DELETING_RESULTS_SUCCESFULLY);
				collect.stream().map(t -> {
					t.setDelete(true);
					return repo.save(t);
				});
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
}
