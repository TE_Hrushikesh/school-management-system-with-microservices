package com.te.exam.examservice;

import java.util.List;

import com.te.exam.examdto.ExamDto;
import com.te.exam.examentity.Exam;

public interface ExamService {

	Exam addresult(ExamDto examDto);

	Exam getUnitTestResult(String studentId, String emailId, String testNumber);

	Exam getSemesterResult(String studentId, String emailId, String testNumber);

	Exam getFinalResult(String studentId, String emailId);

	List<Exam> getAllResults();

	List<Exam> getAllResultsBySchoolId(String schoolId);

	List<Exam> getStudentResultListById(String studentId);

	void deleteResult(String examType, String examNumber);
}
