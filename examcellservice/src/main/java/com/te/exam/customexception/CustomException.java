package com.te.exam.customexception;

@SuppressWarnings("serial")
public class CustomException extends RuntimeException {

	public CustomException(String s) {
		super(s);
	}
}
