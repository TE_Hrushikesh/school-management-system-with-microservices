package com.te.exam.examdto;

import java.util.List;

import lombok.Data;

@Data
public class SchoolDto {

	private String schoolName;
	private String schoolEmail;
	private String schoolAddress;
	private String schoolId;

	private List<String> studentIds;

	private List<StudentDto> students;
}
