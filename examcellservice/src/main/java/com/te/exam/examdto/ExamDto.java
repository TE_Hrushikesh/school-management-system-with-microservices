package com.te.exam.examdto;

import java.time.LocalDate;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamDto {

	private LocalDate date;
	private String examId;
	private String schoolId;
	private String schoolName;
	private String schoolEmail;
	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String examType;
	private Map<String, Integer> subjecMarks;
	private int totalMarksObtained;
	private int totalMarks;
	private double percentage;
	private String result;
}
