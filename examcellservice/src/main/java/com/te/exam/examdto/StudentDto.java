package com.te.exam.examdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {

	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String studentAddress;
	private long mobileNo;
	private boolean schoolAssigned;
}
