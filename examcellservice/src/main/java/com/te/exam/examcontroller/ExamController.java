package com.te.exam.examcontroller;

import static com.te.exam.constants.Constants.ADDING_RESULT;
import static com.te.exam.constants.Constants.DELETING_RESULTS_BY_EXAM_TYPE;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_BY_SCHOOL_ID;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_BY_STUDENT_ID;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_OF_PERTICULAR_STUDENT;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULTS_SUCCESFULLY;
import static com.te.exam.constants.Constants.GETTING_ALL_RESULT_LIST;
import static com.te.exam.constants.Constants.GETTING_EXCEPTION_OF_RESILENCE4J;
import static com.te.exam.constants.Constants.GETTING_FINAL_TEST_RESULT_WITH;
import static com.te.exam.constants.Constants.GETTING_RESULT_LIST_BY_SCHOOL_ID;
import static com.te.exam.constants.Constants.GETTING_RESULT_SUCCESFULLY_FOR_FINAL_TEST;
import static com.te.exam.constants.Constants.GETTING_RESULT_SUCCESFULLY_FOR_SEMISTER;
import static com.te.exam.constants.Constants.GETTING_RESULT_SUCCESFULLY_FOR_UNIT_TEST;
import static com.te.exam.constants.Constants.GETTING_SEMESTER_TEST_RESULT_WITH;
import static com.te.exam.constants.Constants.GETTING_UNIT_TEST_RESULT_WITH;
import static com.te.exam.constants.Constants.RESLUT_DELETED_SUCCESFULLY;
import static com.te.exam.constants.Constants.RESULT_ADDED_SUCCESFULLY;
import static com.te.exam.constants.Constants.SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.exam.examdto.ExamDto;
import com.te.exam.examentity.Exam;
import com.te.exam.examresponse.ExamResponse;
import com.te.exam.examservice.ExamService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/exam")
@Slf4j
public class ExamController {

	@Autowired
	private ExamService service;

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@PostMapping("/addResult")
	public ResponseEntity<ExamResponse> addStudent(@RequestBody ExamDto examDto) {
		log.info(ADDING_RESULT);
		Exam exam = service.addresult(examDto);
		return new ResponseEntity<ExamResponse>(new ExamResponse(false, RESULT_ADDED_SUCCESFULLY, exam), HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getUnitTestResult/{studentId},{emailId},{testNumber}")
	public ResponseEntity<ExamResponse> getUnitTestResult(@PathVariable String studentId, @PathVariable String emailId,
			@PathVariable String testNumber) {
		log.info(GETTING_UNIT_TEST_RESULT_WITH + studentId + " " + emailId + " " + testNumber);
		Exam exam = service.getUnitTestResult(studentId, emailId, testNumber);
		return new ResponseEntity<ExamResponse>(
				new ExamResponse(false, GETTING_RESULT_SUCCESFULLY_FOR_UNIT_TEST + testNumber, exam), HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getSemesterResult/{studentId},{emailId},{testNumber}")
	public ResponseEntity<ExamResponse> getSemesterResult(@PathVariable String studentId, @PathVariable String emailId,
			@PathVariable String testNumber) {
		log.info(GETTING_SEMESTER_TEST_RESULT_WITH + studentId + " " + emailId + " " + testNumber);
		Exam exam = service.getSemesterResult(studentId, emailId, testNumber);
		return new ResponseEntity<ExamResponse>(
				new ExamResponse(false, GETTING_RESULT_SUCCESFULLY_FOR_SEMISTER + testNumber, exam), HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getFinalResult/{studentId},{emailId}")
	public ResponseEntity<ExamResponse> getFinalResult(@PathVariable String studentId, @PathVariable String emailId) {
		log.info(GETTING_FINAL_TEST_RESULT_WITH + studentId + " " + emailId);
		Exam exam = service.getFinalResult(studentId, emailId);
		return new ResponseEntity<ExamResponse>(
				new ExamResponse(false, GETTING_RESULT_SUCCESFULLY_FOR_FINAL_TEST, exam), HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getAllResults")
	public ResponseEntity<ExamResponse> getAllResults() {
		log.info(GETTING_ALL_RESULT_LIST);
		List<Exam> exams = service.getAllResults();
		return new ResponseEntity<ExamResponse>(new ExamResponse(false, GETTING_ALL_RESULTS_SUCCESFULLY, exams),
				HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getAllResultsBySchoolId/{schoolId}")
	public ResponseEntity<ExamResponse> getAllResultsBySchoolId(@PathVariable String schoolId) {
		log.info(GETTING_RESULT_LIST_BY_SCHOOL_ID + schoolId);
		List<Exam> exams = service.getAllResultsBySchoolId(schoolId);
		return new ResponseEntity<ExamResponse>(new ExamResponse(false, GETTING_ALL_RESULTS_BY_SCHOOL_ID, exams),
				HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getStudentResultListById/{studentId}")
	public ResponseEntity<ExamResponse> getStudentResultListById(@PathVariable String studentId) {
		log.info(GETTING_ALL_RESULTS_OF_PERTICULAR_STUDENT + studentId);
		List<Exam> exams = service.getStudentResultListById(studentId);
		return new ResponseEntity<ExamResponse>(new ExamResponse(false, GETTING_ALL_RESULTS_BY_STUDENT_ID, exams),
				HttpStatus.OK);
	}

	@DeleteMapping("/deleteResult/{examType},{examNumber}")
	public ResponseEntity<ExamResponse> deleteResults(@PathVariable String examType, @PathVariable String examNumber) {
		log.info(DELETING_RESULTS_BY_EXAM_TYPE + examType + " & Exam Number : " + examNumber);
		service.deleteResult(examType, examNumber);
		return new ResponseEntity<ExamResponse>(new ExamResponse(false, RESLUT_DELETED_SUCCESFULLY, null),
				HttpStatus.OK);
	}

	public ResponseEntity<ExamResponse> getStudent(Exception e) {
		log.info(GETTING_EXCEPTION_OF_RESILENCE4J);
		return new ResponseEntity<ExamResponse>(
				new ExamResponse(true, SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME2, null), HttpStatus.OK);
	}

}
