package com.te.school.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.te.school.customexception.CustomException;
import com.te.school.schoolresponse.StudentResponse;

@RestControllerAdvice
public class StudentExceptionHandler {

	@ExceptionHandler(CustomException.class)
	public ResponseEntity<StudentResponse> customExceptionHandler(CustomException exception) {
		return new ResponseEntity<StudentResponse>(new StudentResponse(true, exception.getMessage(), null),
				HttpStatus.BAD_REQUEST);
	}
}
