package com.te.school.schoolconstants;

public class Constants {

	public static final String GETTING_ALL_STUDENTS_BY_SCHOOL_ID = "Getting All Students By SchoolId";

	public static final String UPDATE_SCHOOL_SUCCESFULLY = "Update School Succesfully";

	public static final String GETTING_STUDENTS_BY_SCHOOL_ID_AND_STANDARD = "Getting Students By School Id And Standard";
	
	public static final String GETTING_SCHOOL_BY_STUDENT_ID_USING_FEIGN_CLIENTS = "Getting School By Student Id Using Feign Clients";

	public static final String GETTING_SCHOOL_BY_SCHOOL_ID_USING_FEIGN_CLIENTS = "Getting School By School Id Using Feign Clients";

	public static final String GETTING_STUDENT_RESPECT_TO_STANDARD_AND_SCHOOL_ID = "Getting Student Respect To Standard And SchoolId";

	public static final String STANDARD2 = " Standard";

	public static final String GETTING_STUDENTS_OF = "Getting Students Of ";

	public static final String GETTING_STUDENTS_BY_SCHOOL_ID = "Getting Students By School Id";

	public static final String SCHOOL_UPDATED_SUCCESFULLY = "School Updated Succesfully";

	public static final String UPDATING_SCHOOL_DETAILS = "Updating School Details";

	public static final String GETTING_ALL_SCHOOL_DETAILS_SUCCESFULLY = "Getting All School Details Succesfully";

	public static final String GETTING_ALL_SCHOOL_DETAILS = "Getting All School Details";

	public static final String DELETE_SCHOOL_DETAIL_SUCCESFULLY = "Delete School Detail Succesfully";

	public static final String DELETING_SCHOOL_DETAILS = "Deleting School Details";

	public static final String GETTING_SCHOOL_DETAILS_SUCCESFULLY = "Getting School Details Succesfully";

	public static final String GETTING_SCHOOL_DETAILS = "Getting School Details";

	public static final String SCHOOL_ADDED_SUCCESFULLY = "School Added Succesfully";

	public static final String ADDING_SCHOOL = "Adding School";

	public static final String WHICH_SCHOOL_YOU_WANT_TO_UPDATE_IS_NOT_PRESENT = "Which School You Want To Update Is Not Present";

	public static final String SCHOOL_DELETED_SUCCESFULLY = "School Deleted Succesfully";

	public static final String SCHOOL_IS_NOT_PRESENT_ON_THIS_ID = "School IS Not Present On This Id";

	public static final String SCHOOL_WHICH_YOU_WANT_IS_ALLREADY_PRESENT = "School Which You Want Is Allready Present";
}
