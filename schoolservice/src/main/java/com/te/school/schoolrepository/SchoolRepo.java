package com.te.school.schoolrepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.school.schoolentity.School;

@Repository
public interface SchoolRepo extends JpaRepository<School, Integer> {

	Optional<School> findBySchoolEmail(String schoolEmail);

	Optional<School> findBySchoolId(String schoolId);

}
