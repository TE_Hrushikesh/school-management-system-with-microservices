package com.te.school.schooldto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class SchoolDto {

	private String schoolName;
	private String schoolEmail;
	private String schoolAddress;
	private String schoolId;

	private List<String> studentIds;

	private List<StudentDto> students;
}