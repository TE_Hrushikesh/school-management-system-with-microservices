package com.te.school.schoolservice;

import java.util.List;

import com.te.school.schooldto.SchoolDto;
import com.te.school.schooldto.StudentDto;
import com.te.school.schoolentity.School;

public interface SchoolService {

	School addSchool(SchoolDto schoolDto);

	School getSchool(String schoolId);

	void deleteSchool(String schoolId);

	List<School> getAllSchool();

	List<StudentDto> getStudentByStandard(String standard, String schoolId);

	School updateSchool(SchoolDto schoolDto);

	List<StudentDto> getAllBySchoolId(String schoolId);

	SchoolDto getSchoolByStudId(String id);

}
