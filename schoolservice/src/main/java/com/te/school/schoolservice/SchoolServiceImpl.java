package com.te.school.schoolservice;

import static com.te.school.schoolconstants.Constants.GETTING_ALL_SCHOOL_DETAILS;
import static com.te.school.schoolconstants.Constants.GETTING_ALL_STUDENTS_BY_SCHOOL_ID;
import static com.te.school.schoolconstants.Constants.GETTING_SCHOOL_DETAILS_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.GETTING_STUDENTS_BY_SCHOOL_ID_AND_STANDARD;
import static com.te.school.schoolconstants.Constants.SCHOOL_ADDED_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.SCHOOL_DELETED_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.SCHOOL_IS_NOT_PRESENT_ON_THIS_ID;
import static com.te.school.schoolconstants.Constants.UPDATE_SCHOOL_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.WHICH_SCHOOL_YOU_WANT_TO_UPDATE_IS_NOT_PRESENT;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.school.customexception.CustomException;
import com.te.school.schoolcontroller.StudFiegn;
import com.te.school.schooldto.SchoolDto;
import com.te.school.schooldto.StudentDto;
import com.te.school.schoolentity.School;
import com.te.school.schoolrepository.SchoolRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SchoolServiceImpl implements SchoolService {

	@org.springframework.beans.factory.annotation.Autowired(required = true)
	private StudFiegn feign;

	@Autowired
	private SchoolRepo repo;

	@Override
	public School addSchool(SchoolDto schoolDto) {
		try {
			Optional<School> school = repo.findBySchoolEmail(schoolDto.getSchoolEmail());
			if (school.isPresent()) {
				feign.assignSchool(schoolDto.getStudentIds());
				school.get().setStudentIds(Arrays.asList(schoolDto.getStudentIds(), school.get().getStudentIds())
						.stream().flatMap(t -> t.stream()).collect(Collectors.toList()));
				log.info(SCHOOL_ADDED_SUCCESFULLY);
				return repo.save(school.get());
			} else {
				School school1 = new School();
				BeanUtils.copyProperties(schoolDto, school1);
//				repo.findAll().stream().filter(t -> !t.getStudentIds().stream().filter(t ->t.equalsIgnoreCase(schoolDto.getStudentIds().stream().map(t -> t).collect(Collectors.joining())) ))
				feign.assignSchool(schoolDto.getStudentIds());
				log.info(SCHOOL_ADDED_SUCCESFULLY);
				School save = repo.save(school1);
				return save;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public School getSchool(String schoolId) {
		try {
			Optional<School> school = repo.findBySchoolId(schoolId);
			if (!school.isPresent() && school.get().isDelete() == true) {
				log.error(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
				throw new CustomException(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
			} else {
				log.info(GETTING_SCHOOL_DETAILS_SUCCESFULLY);
				return school.get();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void deleteSchool(String schoolId) {
		try {
			Optional<School> school = repo.findBySchoolId(schoolId);
			if (!school.isPresent()) {
				log.error(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
				throw new CustomException(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
			} else if (school.get().isDelete() == true) {
				log.error(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
				throw new CustomException(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
			} else {
				log.info(SCHOOL_DELETED_SUCCESFULLY);
				school.get().setDelete(true);
				repo.save(school.get());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public List<School> getAllSchool() {
		try {
			List<School> schools = repo.findAll().stream().filter(t -> t.isDelete() == false)
					.collect(Collectors.toList());
			log.info(GETTING_ALL_SCHOOL_DETAILS);
			return schools;

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<StudentDto> getStudentByStandard(String standard, String schoolId) {
		try {
			Optional<School> School = repo.findBySchoolId(schoolId);
			if (!School.isPresent() && School.get().isDelete() == true) {
				log.error(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID + schoolId);
				throw new CustomException(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
			} else {
				log.info(GETTING_STUDENTS_BY_SCHOOL_ID_AND_STANDARD);
				return feign.getStudentsById(School.get().getStudentIds()).stream()
						.filter(t -> t.getStandard().equalsIgnoreCase(standard)).collect(Collectors.toList());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public School updateSchool(SchoolDto schoolDto) {
		try {
			Optional<School> school = repo.findBySchoolId(schoolDto.getSchoolId());

			if (!school.isPresent() && school.get().isDelete() == true) {
				log.error(WHICH_SCHOOL_YOU_WANT_TO_UPDATE_IS_NOT_PRESENT);
				throw new CustomException(WHICH_SCHOOL_YOU_WANT_TO_UPDATE_IS_NOT_PRESENT);
			} else {
				BeanUtils.copyProperties(schoolDto, school.get());
				log.info(UPDATE_SCHOOL_SUCCESFULLY);
				return school.get();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<StudentDto> getAllBySchoolId(String schoolId) {
		try {
			Optional<School> school = repo.findBySchoolId(schoolId);
			if (!school.isPresent() && school.get().isDelete() == true) {
				log.error(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
				throw new CustomException(SCHOOL_IS_NOT_PRESENT_ON_THIS_ID);
			} else {
				List<StudentDto> students = feign.getStudentsById(school.get().getStudentIds());
				log.info(GETTING_ALL_STUDENTS_BY_SCHOOL_ID);
				return students;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public SchoolDto getSchoolByStudId(String id) {
		SchoolDto school = new SchoolDto();
		try {
			Optional<School> findFirst = repo.findAll().stream()
					.filter(t -> t.getStudentIds().stream().anyMatch(q -> q.equalsIgnoreCase(id))).findFirst();
			if (findFirst.isPresent() && findFirst.get().isDelete() == false) {
				school.setSchoolId(findFirst.get().getSchoolId());
				school.setSchoolAddress(findFirst.get().getSchoolAddress());
				school.setSchoolEmail(findFirst.get().getSchoolEmail());
				school.setSchoolName(findFirst.get().getSchoolName());
				log.info(GETTING_SCHOOL_DETAILS_SUCCESFULLY);
				return school;
			} else {
				log.info(GETTING_SCHOOL_DETAILS_SUCCESFULLY);
				return null;
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw e;
		}
	}
}
