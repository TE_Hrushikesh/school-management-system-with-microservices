package com.te.school.schoolcontroller;

import static com.te.school.schoolconstants.Constants.GETTING_SCHOOL_BY_SCHOOL_ID_USING_FEIGN_CLIENTS;
import static com.te.school.schoolconstants.Constants.GETTING_SCHOOL_BY_STUDENT_ID_USING_FEIGN_CLIENTS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.school.schooldto.SchoolDto;
import com.te.school.schoolentity.School;
import com.te.school.schoolservice.SchoolService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/schoolFeign")
public class FeignController {

	@Autowired
	private SchoolService service;

	@GetMapping("/getSchoolBiStudId/{id}")
	public SchoolDto getSchoolByStudentId(@PathVariable String id) {
		SchoolDto school = service.getSchoolByStudId(id);
		log.info(GETTING_SCHOOL_BY_STUDENT_ID_USING_FEIGN_CLIENTS);
		return school;
	}

	@GetMapping("/getSchool/{schoolId}")
	public School getSchool(@PathVariable String schoolId) {
		log.info(GETTING_SCHOOL_BY_SCHOOL_ID_USING_FEIGN_CLIENTS);
		return service.getSchool(schoolId);
	}
}
