package com.te.school.schoolcontroller;

import static com.te.school.schoolconstants.Constants.ADDING_SCHOOL;
import static com.te.school.schoolconstants.Constants.DELETE_SCHOOL_DETAIL_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.DELETING_SCHOOL_DETAILS;
import static com.te.school.schoolconstants.Constants.GETTING_ALL_SCHOOL_DETAILS;
import static com.te.school.schoolconstants.Constants.GETTING_ALL_SCHOOL_DETAILS_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.GETTING_SCHOOL_DETAILS;
import static com.te.school.schoolconstants.Constants.GETTING_SCHOOL_DETAILS_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.GETTING_STUDENTS_BY_SCHOOL_ID;
import static com.te.school.schoolconstants.Constants.GETTING_STUDENTS_OF;
import static com.te.school.schoolconstants.Constants.GETTING_STUDENT_RESPECT_TO_STANDARD_AND_SCHOOL_ID;
import static com.te.school.schoolconstants.Constants.SCHOOL_ADDED_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.SCHOOL_UPDATED_SUCCESFULLY;
import static com.te.school.schoolconstants.Constants.STANDARD2;
import static com.te.school.schoolconstants.Constants.UPDATING_SCHOOL_DETAILS;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.school.schooldto.SchoolDto;
import com.te.school.schooldto.StudentDto;
import com.te.school.schoolentity.School;
import com.te.school.schoolresponse.StudentResponse;
import com.te.school.schoolservice.SchoolService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/school")
@Slf4j
public class SchoolController {

	private static final String SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME = "SomeThing Went Wrong Please Try After Some Time!!!!";
	@Autowired
	private SchoolService service;

	@PostMapping("/addSchool")
	@Retry(name = "UserService", fallbackMethod = "getUserData")
	@CircuitBreaker(name = "UserService", fallbackMethod = "getUserData")
	public ResponseEntity<StudentResponse> addSchool(@RequestBody SchoolDto schoolDto) {
		log.info(ADDING_SCHOOL);
		School school = service.addSchool(schoolDto);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, SCHOOL_ADDED_SUCCESFULLY, school),
				HttpStatus.OK);
	}

	@RateLimiter(name = "UserService", fallbackMethod = "getUserData")
	@GetMapping("/getSchool/{schoolId}")
	public ResponseEntity<StudentResponse> getSchool(@PathVariable String schoolId) {
		log.info(GETTING_SCHOOL_DETAILS + schoolId);
		School school = service.getSchool(schoolId);
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(false, GETTING_SCHOOL_DETAILS_SUCCESFULLY, school), HttpStatus.OK);
	}

	@DeleteMapping("/deleteSchool/{schoolId}")
	public ResponseEntity<StudentResponse> deleteSchool(@PathVariable String schoolId) {
		log.info(DELETING_SCHOOL_DETAILS + schoolId);
		service.deleteSchool(schoolId);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, DELETE_SCHOOL_DETAIL_SUCCESFULLY, null),
				HttpStatus.OK);
	}

	@RateLimiter(name = "UserService", fallbackMethod = "getUserData")
	@GetMapping("/getAll")
	public ResponseEntity<StudentResponse> getAllStudent() {
		log.info(GETTING_ALL_SCHOOL_DETAILS);
		List<School> school = service.getAllSchool();
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(false, GETTING_ALL_SCHOOL_DETAILS_SUCCESFULLY, school), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<StudentResponse> updateSchool(@RequestBody SchoolDto schoolDto) {
		log.info(UPDATING_SCHOOL_DETAILS);
		School school = service.updateSchool(schoolDto);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, SCHOOL_UPDATED_SUCCESFULLY, school),
				HttpStatus.OK);
	}

	@Retry(name = "UserService", fallbackMethod = "getUserData")
	@RateLimiter(name = "UserService", fallbackMethod = "getUserData")
	@CircuitBreaker(name = "UserService", fallbackMethod = "getUserData")
	@GetMapping("/getStudentsBySchoolId/{schoolId}")
	public ResponseEntity<StudentResponse> getAllBySchoolId(@PathVariable String schoolId) {
		log.info(GETTING_STUDENTS_BY_SCHOOL_ID);
		List<StudentDto> students = service.getAllBySchoolId(schoolId);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, GETTING_STUDENTS_BY_SCHOOL_ID, students),
				HttpStatus.OK);
	}

	@Retry(name = "UserService", fallbackMethod = "getUserData")
	@RateLimiter(name = "UserService", fallbackMethod = "getUserData")
	@CircuitBreaker(name = "UserService", fallbackMethod = "getUserData")
	@GetMapping("/getStandard/{standard},{schoolId}")
	public ResponseEntity<StudentResponse> getStudentListWithstandard(@PathVariable String standard,
			@PathVariable String schoolId) {
		log.info(GETTING_STUDENT_RESPECT_TO_STANDARD_AND_SCHOOL_ID);
		List<StudentDto> student = service.getStudentByStandard(standard, schoolId);
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(false, GETTING_STUDENTS_OF + standard + STANDARD2, student), HttpStatus.OK);
	}

	public ResponseEntity<StudentResponse> getUserData(Exception e) {
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(true, SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME, null), HttpStatus.OK);
	}
}
