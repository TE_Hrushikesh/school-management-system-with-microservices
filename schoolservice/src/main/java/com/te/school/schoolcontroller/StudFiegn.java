package com.te.school.schoolcontroller;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.te.school.schooldto.StudentDto;

@FeignClient(name = "STUDENT-SERVICES")
public interface StudFiegn {

	@PostMapping("/feignStudent/assignSchool")
	public void assignSchool(@RequestBody List<String> studentId);

	@PostMapping("/feignStudent/getStudentsById")
	public List<StudentDto> getStudentsById(@RequestBody List<String> studentId);
}
