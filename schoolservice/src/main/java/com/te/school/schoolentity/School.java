package com.te.school.schoolentity;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.te.school.schoolcontroller.ListToString;
import com.te.school.schooldto.StudentDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class School {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String schoolName;
	private String schoolEmail;
	private String schoolAddress;
	private String schoolId;
	private boolean isDelete;

	@Convert(converter = ListToString.class)
	private List<String> studentIds;

	@Transient
	private List<StudentDto> students;
}
