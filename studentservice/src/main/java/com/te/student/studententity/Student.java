package com.te.student.studententity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.te.student.studentdto.SchoolDto;

import lombok.Data;

@Data
@javax.persistence.Entity
@JsonInclude(value = Include.NON_NULL)
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String studentAddress;
	private long mobileNo;
	private boolean schoolAssigned;
	private boolean isDelete;

	@Transient
	private SchoolDto school;

}
