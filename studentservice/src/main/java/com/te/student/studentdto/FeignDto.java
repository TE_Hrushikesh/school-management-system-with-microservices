package com.te.student.studentdto;

import lombok.Data;

@Data
public class FeignDto {

	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String studentAddress;
	private long mobileNo;
	private boolean schoolAssigned;
}
