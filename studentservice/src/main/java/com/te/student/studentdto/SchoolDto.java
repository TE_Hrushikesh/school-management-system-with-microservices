package com.te.student.studentdto;

import lombok.Data;

@Data
public class SchoolDto {

	private String schoolId;
	private String SchoolName;
	private String schoolEmail;
	private String schoolAddress;

}