package com.te.student.studentdto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class StudentDto {

	private String studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmail;
	private String standard;
	private String studentAddress;
	private long mobileNo;
	private boolean schoolAssigned;
	
	private SchoolDto school;
}
