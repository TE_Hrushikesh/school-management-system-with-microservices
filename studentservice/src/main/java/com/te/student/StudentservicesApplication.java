package com.te.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@OpenAPIDefinition
public class StudentservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentservicesApplication.class, args);
	}

}
