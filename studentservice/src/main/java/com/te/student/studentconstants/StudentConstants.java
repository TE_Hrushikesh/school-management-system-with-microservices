package com.te.student.studentconstants;

public class StudentConstants {
	

	public static final String SO_MANY_REQUESTS_YOU_SENT_PLEASE_WAIT_FOR_SOME_TIME_TRY_AGAIN = "So Many Requests You Sent Please Wait For Some Time & Try Again!!!";
	
	public static final String SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME = "SomeThing Went Wrong Please Try After Some Time!!!";
	
	public static final String GETTING_ALL_STUDENTS_LIST_BY_LIST_OF_STUDENT_ID = "Getting All Students List By List Of Student Id ";

	public static final String GETTING_STUDENT_SUCCESSFULLY = "Getting Student Successfully";

	public static final String SCHOOL_ASSIGNED_SUCCESFULLY = "School Assigned Succesfully";

	public static final String ENTER_THE_RIGHT_STUDENT_ID = "Enter The Right Student Id";

	public static final String GETTING_STUDENTS = "Getting Students";
	
	public static final String GETTING_STUDENTS_BY_ANY_WORD = "Getting Students By Any word";

	public static final String UPDATE_STUDENT_DETAILS_SUCCESFULLY = "Update Student Details Succesfully";

	public static final String STUDENT_IS_NOT_PRESENT_WHICH_YOU_WANT_TO_UPDATE = "Student Is Not Present Which You Want To Update";

	public static final String GETTING_ALL_STUDENT_DETAILS_SUCCESFULLY = "Getting All Student Details Succesfully";

	public static final String NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT = "No One Student Is Not Present At The Moment";

	public static final String DELETING_STUDENT_DETAILS_SUCCESFULLY = "Deleting Student Details Succesfully";

	public static final String GETTING_STUDENT_DETAILS_SUCCESFULLY = "Getting Student Details Succesfully";

	public static final String STUDENT_IS_NOT_PRESENT_ON_THIS_ID = "Student Is Not Present On This Id";

	public static final String SOMETHING_WENT_WRONG = "Something Went Wrong";

	public static final String STUDENT_WHICH_YOU_WANT_TO_ADD_IS_ALLREADY_PRESENT = "Student Which You Want To Add Is Allready Present";

	public static final String STUDENT_UPDATED_SUCCESFULLY = "Student Updated Succesfully";

	public static final String UPDATING_STUDENT_DETAILS = "Updating Student Details";

	public static final String GETTING_ALL_DETAILS = "Getting All Details";

	public static final String DELETE_STUDENT_SUCCESFULLY = "Delete Student Succesfully";

	public static final String DELETING_STUDENT_DETAILS = "Deleting Student details";

	public static final String GETTING_STUDENT_DETAIL_SUCCESFULLY = "Getting Student Detail Succesfully";

	public static final String GETTING_STUDENT_DETAILS = "Getting Student Details : ";

	public static final String STUDENT_ADDED_SUCCESFULLY = "Student Added Succesfully";

	public static final String ADDING_STUDENT = "Adding Student";
}
