package com.te.student.studentcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.student.studentdto.FeignDto;
import com.te.student.studentdto.StudentDto;
import com.te.student.studentservice.StudentService;

@RestController
@RequestMapping("/feignStudent")
public class FeignController {

	@Autowired
	private StudentService service;

	@PostMapping("/assignSchool")
	public void assignSchool(@RequestBody List<String> studentId) {
		service.assignSchool(studentId);
	}

	@PostMapping("/getStudentsById")
	public List<FeignDto> getStudentsById(@RequestBody List<String> studentId) {
		return service.getStudentsById(studentId);
	}

	@GetMapping("/get/{studentId}")
	public StudentDto getStudent(@PathVariable String studentId) {
		return service.getStudentbyId(studentId);
	}
}
