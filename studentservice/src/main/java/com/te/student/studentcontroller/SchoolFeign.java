package com.te.student.studentcontroller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.te.student.studentdto.SchoolDto;


@FeignClient(name="SCHOOL-SERVICE")
public interface SchoolFeign {

	@GetMapping("/schoolFeign/getSchoolBiStudId/{id}")
	public SchoolDto getSchoolByStudentId(@PathVariable String id);

}
