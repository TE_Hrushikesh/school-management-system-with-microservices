package com.te.student.studentcontroller;

import static com.te.student.studentconstants.StudentConstants.ADDING_STUDENT;
import static com.te.student.studentconstants.StudentConstants.DELETE_STUDENT_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.DELETING_STUDENT_DETAILS;
import static com.te.student.studentconstants.StudentConstants.GETTING_ALL_DETAILS;
import static com.te.student.studentconstants.StudentConstants.GETTING_ALL_STUDENT_DETAILS_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENTS;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENTS_BY_ANY_WORD;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENT_DETAILS;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENT_DETAIL_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME;
import static com.te.student.studentconstants.StudentConstants.STUDENT_ADDED_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.STUDENT_UPDATED_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.UPDATING_STUDENT_DETAILS;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.student.studentdto.StudentDto;
import com.te.student.studententity.Student;
import com.te.student.studentresponse.StudentResponse;
import com.te.student.studentservice.StudentService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/student")
@Slf4j
public class StudentController {

	@Autowired
	private StudentService service;

	@PostMapping("/add")
	public ResponseEntity<StudentResponse> addStudent(@RequestBody StudentDto studentDto) {
		log.info(ADDING_STUDENT);
		Student student = service.addStudent(studentDto);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, STUDENT_ADDED_SUCCESFULLY, student),
				HttpStatus.OK);
	}

	@CircuitBreaker(name = "UserService", fallbackMethod = "getStudent")
	@Retry(name = "UserService", fallbackMethod = "getStudent")
	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/get/{studentId}")
	public ResponseEntity<StudentResponse> getStudent(@PathVariable String studentId) {
		log.info(GETTING_STUDENT_DETAILS + studentId);
		StudentDto student = service.getStudent(studentId);
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(false, GETTING_STUDENT_DETAIL_SUCCESFULLY, student), HttpStatus.OK);
	}

	@DeleteMapping("/delete/{studentId}")
	public ResponseEntity<StudentResponse> deleteStudent(@PathVariable String studentId) {
		log.info(DELETING_STUDENT_DETAILS + studentId);
		service.deleteStudent(studentId);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, DELETE_STUDENT_SUCCESFULLY, null),
				HttpStatus.OK);
	}

	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getAll")
	public ResponseEntity<StudentResponse> getAllStudent() {
		log.info(GETTING_ALL_DETAILS);
		List<Student> student = service.getAllStudent();
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(false, GETTING_ALL_STUDENT_DETAILS_SUCCESFULLY, student), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<StudentResponse> updateStudent(@RequestBody StudentDto studentDto) {
		log.info(UPDATING_STUDENT_DETAILS);
		Student student = service.updateStudent(studentDto);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, STUDENT_UPDATED_SUCCESFULLY, student),
				HttpStatus.OK);
	}

	@RateLimiter(name = "UserService", fallbackMethod = "getStudent")
	@GetMapping("/getStudentsByAnyWord/{str}")
	public ResponseEntity<StudentResponse> getStudentsByAnyWord(@PathVariable String str) {
		log.info(GETTING_STUDENTS_BY_ANY_WORD);
		List<Student> student = service.getStudentsByAnyWord(str);
		return new ResponseEntity<StudentResponse>(new StudentResponse(false, GETTING_STUDENTS, student),
				HttpStatus.OK);
	}

	public ResponseEntity<StudentResponse> getStudent(Exception e) {
		return new ResponseEntity<StudentResponse>(
				new StudentResponse(true, SOME_THING_WENT_WRONG_PLEASE_TRY_AFTER_SOME_TIME, null), HttpStatus.OK);
	}

}
