package com.te.student.studentservice;

import static com.te.student.studentconstants.StudentConstants.DELETING_STUDENT_DETAILS_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.ENTER_THE_RIGHT_STUDENT_ID;
import static com.te.student.studentconstants.StudentConstants.GETTING_ALL_STUDENTS_LIST_BY_LIST_OF_STUDENT_ID;
import static com.te.student.studentconstants.StudentConstants.GETTING_ALL_STUDENT_DETAILS_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENT_DETAILS_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.GETTING_STUDENT_SUCCESSFULLY;
import static com.te.student.studentconstants.StudentConstants.NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT;
import static com.te.student.studentconstants.StudentConstants.SCHOOL_ASSIGNED_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.STUDENT_ADDED_SUCCESFULLY;
import static com.te.student.studentconstants.StudentConstants.STUDENT_IS_NOT_PRESENT_ON_THIS_ID;
import static com.te.student.studentconstants.StudentConstants.STUDENT_IS_NOT_PRESENT_WHICH_YOU_WANT_TO_UPDATE;
import static com.te.student.studentconstants.StudentConstants.STUDENT_WHICH_YOU_WANT_TO_ADD_IS_ALLREADY_PRESENT;
import static com.te.student.studentconstants.StudentConstants.UPDATE_STUDENT_DETAILS_SUCCESFULLY;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.student.studentcontroller.SchoolFeign;
import com.te.student.studentcustomexception.StudentCustomException;
import com.te.student.studentdto.FeignDto;
import com.te.student.studentdto.SchoolDto;
import com.te.student.studentdto.StudentDto;
import com.te.student.studententity.Student;
import com.te.student.studentrepository.StudentRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepo repo;

	@Autowired
	private SchoolFeign feign;

	@Override
	public Student addStudent(StudentDto studentDto) {
		try {
			Optional<Student> findByStudent = repo.findByStudentEmail(studentDto.getStudentEmail());
			if (findByStudent.isPresent()) {
				log.error(STUDENT_WHICH_YOU_WANT_TO_ADD_IS_ALLREADY_PRESENT);
				throw new StudentCustomException(STUDENT_WHICH_YOU_WANT_TO_ADD_IS_ALLREADY_PRESENT);
			} else {
				Student student = new Student();
				BeanUtils.copyProperties(studentDto, student);
				String s = "stud0" + (repo.findAll().stream().count() + 1);
				if (repo.findAll().stream().anyMatch(t -> t.getStudentId().equalsIgnoreCase(s))) {
					String collect = repo.findAll().stream().filter(t -> t.getStudentId().equalsIgnoreCase(s))
							.map(t -> {
								String s1 = "stud0" + (repo.findAll().stream().count() + 2);
								return s1;
							}).collect(Collectors.joining());
					student.setStudentId(collect);
				} else {

					student.setStudentId(s);
				}
				Student save = repo.save(student);
				log.info(STUDENT_ADDED_SUCCESFULLY);
				return save;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public StudentDto getStudent(String id) {
		try {
			Optional<Student> student = repo.findAll().stream()
					.filter(t -> t.getStudentId().equalsIgnoreCase(id) && t.isDelete() == false).findFirst();
			if (!student.isPresent()) {
				log.error(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
				throw new StudentCustomException(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
			} else {

				StudentDto stud = new StudentDto();
				BeanUtils.copyProperties(student.get(), stud);
				SchoolDto school = feign.getSchoolByStudentId(id);
				stud.setSchool(school);
				log.info(GETTING_STUDENT_DETAILS_SUCCESFULLY);
				return stud;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void deleteStudent(String id) {
		try {
			Optional<Student> student = repo.findAll().stream().filter(t -> t.getStudentId().equalsIgnoreCase(id)&&t.isDelete()==false)
					.findFirst();
			if (!student.isPresent()) {
				log.error(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
				throw new StudentCustomException(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
			} else {
				log.info(DELETING_STUDENT_DETAILS_SUCCESFULLY);
				student.get().setDelete(true);
				repo.save(student.get());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<Student> getAllStudent() {
		try {
			List<Student> studentList = repo.findAll();
			if (studentList.isEmpty()) {
				log.error(NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT);
				throw new StudentCustomException(NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT);
			} else {
				log.info(GETTING_ALL_STUDENT_DETAILS_SUCCESFULLY);
				return studentList.stream().filter(t -> t.isDelete() == false).collect(Collectors.toList());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public Student updateStudent(StudentDto studentDto) {
		try {
			Optional<Student> student = repo.findByStudentEmail(studentDto.getStudentEmail());
			if (!student.isPresent()||student.get().isDelete()==true) {
				log.error(STUDENT_IS_NOT_PRESENT_WHICH_YOU_WANT_TO_UPDATE);
				throw new StudentCustomException(STUDENT_IS_NOT_PRESENT_WHICH_YOU_WANT_TO_UPDATE);
			} else {
				BeanUtils.copyProperties(studentDto, student.get());
				Student save = repo.save(student.get());
				log.info(UPDATE_STUDENT_DETAILS_SUCCESFULLY);
				return save;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<Student> getStudentsByAnyWord(String str) {
		try {
			List<Student> students = repo
					.findAllByStudentFirstNameContainingIgnoreCaseOrStudentLastNameContainingIgnoreCaseOrStudentEmailContainingIgnoreCaseOrStudentAddressContainingIgnoreCase(
							str, str, str, str);

//			List<Student> collect = repo.findAll().stream().filter(t ->t.getStudentFirstName().contains(str)||t.getStudentLastName().contains(str)||t.getStudentId().contains(str)||t.getStudentEmail().contains(str)||t.getStudentAddress().contains(str)||t.getStandard().contains(str)).collect(Collectors.toList());
			if (students.isEmpty()) {
				log.error(NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT);
				throw new StudentCustomException(NO_ONE_STUDENT_IS_NOT_PRESENT_AT_THE_MOMENT);
			} else {
				log.info(GETTING_STUDENT_SUCCESSFULLY);
				return students.stream().filter(t -> t.isDelete() == false).collect(Collectors.toList());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void assignSchool(List<String> studentId) {
		try {
			List<Student> collect = repo.findByStudentIdIn(studentId);
			List<Student> student = collect.stream().filter(t -> t.isDelete()==false).collect(Collectors.toList());
			student.stream().forEach(t -> System.out.println(t));
			if (student.isEmpty()) {
				log.error(ENTER_THE_RIGHT_STUDENT_ID);
				throw new StudentCustomException(ENTER_THE_RIGHT_STUDENT_ID);
			} else {
				List<Student> students = student.stream().filter(t -> t.isDelete() == false)
						.collect(Collectors.toList());
				for (Student student2 : students) {
					student2.setSchoolAssigned(true);
					log.info(SCHOOL_ASSIGNED_SUCCESFULLY);
					repo.save(student2);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<FeignDto> getStudentsById(List<String> studentId) {
		try {
			List<Student> students = repo.findByStudentIdIn(studentId);
			log.info(GETTING_ALL_STUDENTS_LIST_BY_LIST_OF_STUDENT_ID + studentId);
			return students.stream().filter(t -> t.isDelete()==false).map(t -> {
				FeignDto dto = new FeignDto();
				BeanUtils.copyProperties(t, dto);
				return dto;
			}).collect(Collectors.toList());

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public StudentDto getStudentbyId(String studentId) {
		try {
			Optional<Student> student = repo.findAll().stream()
					.filter(t -> t.getStudentId().equalsIgnoreCase(studentId)&&t.isDelete()==false).findFirst();
			if (!student.isPresent()) {
				log.error(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
				throw new StudentCustomException(STUDENT_IS_NOT_PRESENT_ON_THIS_ID);
			} else {

				log.info(GETTING_STUDENT_DETAILS_SUCCESFULLY);
				StudentDto stud = new StudentDto();
				BeanUtils.copyProperties(student.get(), stud);
				return stud;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
}
