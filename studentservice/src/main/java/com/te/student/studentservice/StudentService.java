package com.te.student.studentservice;

import java.util.List;

import com.te.student.studentdto.FeignDto;
import com.te.student.studentdto.StudentDto;
import com.te.student.studententity.Student;

public interface StudentService {

	Student addStudent(StudentDto studentDto);

	StudentDto getStudent(String studentId);

	void deleteStudent(String studentId);

	List<Student> getAllStudent();

	Student updateStudent(StudentDto studentDto);

	List<Student> getStudentsByAnyWord(String str);

	void assignSchool(List<String> studentId);

	List<FeignDto> getStudentsById(List<String> studentId);

	StudentDto getStudentbyId(String studentId);

}
